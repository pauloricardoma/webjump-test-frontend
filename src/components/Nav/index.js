import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import { NavContainer } from './styles';

import CategoriesService from '../../services/CategoriesService';

export default function Nav() {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    async function loadCategories() {
      try {
        const response = await CategoriesService.listCategories();

        setCategories(response.items);
      } catch (err) {
        console.log('Caiu no catch', err);
      }
    }
    loadCategories();
  }, []);

  return (
    <NavContainer>
      <div className="container">
        <Link to="/">PÁGINA INICIAL</Link>
        {categories.map((category) => (
          <Link
            name={category.name}
            key={category.id}
            to={`/${category.id}`}
          >
            {category.name.toUpperCase()}
          </Link>
        ))}
        <Link to="/">CONTATO</Link>
      </div>
    </NavContainer>
  );
}
