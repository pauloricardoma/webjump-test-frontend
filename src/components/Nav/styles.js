import styled from 'styled-components';

export const NavContainer = styled.nav`
  background: ${({ theme }) => theme.colors.red.dark};
  width: 100%;
  height: 54px;
  display: flex;
  justify-content: center;
  align-items: center;

  @media (max-width: 768px) {
    display: none;
  }

  .container {
    width: 100%;
    max-width: ${({ theme }) => theme.responsive[1440]};
    display: flex;
    justify-content: start;
    align-items: center;
    margin: 0 20px;
  }

  a {
    color: #FFFFFF;
    font-weight: 800;
    text-decoration: none;

    & {
      margin-right: 75px;
    }
  }
`;
