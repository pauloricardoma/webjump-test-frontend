import React from 'react';

import { Container, FooterContainer } from './styles';

export default function Footer() {
  return (
    <Container>
      <FooterContainer>
        <small>
          Desenvolvido por Paulo Ricardo M. de Almeida - Dev Jr WebJump
        </small>
      </FooterContainer>
    </Container>
  );
}
