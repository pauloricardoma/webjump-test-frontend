import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const FooterContainer = styled.footer`
  background: ${({ theme }) => theme.colors.red.dark};
  width: 100%;
  max-width: ${({ theme }) => theme.responsive[1440]};
  margin: 0 20px;
  height: 176px;
  display: flex;
  align-items: end;
  justify-content: center;

  small {
    color: #FFFFFF;
    font-size: 14px;
    margin-bottom: 14px;
  }
`;
