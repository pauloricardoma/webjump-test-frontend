import styled, { css } from 'styled-components';

export default styled.button`
  background: ${({ theme }) => theme.colors.red.dark};
  color: #FFFFFF;
  border: none;
  width: 110px;
  height: 44px;
  font-weight: 800;
  font-size: 16px;
  transform: background 0.3s ease-in;

  &:hover {
    background: ${({ theme }) => theme.colors.red.mid};
  }

  &:active {
    background: ${({ theme }) => theme.colors.red.light};
  }

  ${({ theme, category }) => category && css`
    background: ${theme.colors.green.button};
  `}

  &.blue {
    background: ${({ theme }) => theme.colors.blue.button};
  }
`;
