import styled from 'styled-components';

export default styled.input`
  width: 100%;
  max-width: 414px;
  min-width: 414px;
  height: 44px;
  padding: 0 16px;
`;
