import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import { ThemeProvider } from 'styled-components';

import GlobalStyle from '../../assets/styles/global';
import defaultTheme from '../../assets/styles/themes/default';
import { Container } from './styles';

import Routes from '../../routes';
import TopHeader from '../TopHeader';
import Footer from '../Footer';

export default function App() {
  return (
    <BrowserRouter>
      <ThemeProvider theme={defaultTheme}>
        <GlobalStyle />
        <Container>
          <TopHeader
            title1="Acesse sua Conta"
            title2="Cadastra-se"
          />
          <Routes />
          <Footer />
        </Container>
      </ThemeProvider>
    </BrowserRouter>
  );
}
