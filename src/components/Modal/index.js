import React from 'react';
import PropTypes from 'prop-types';

import { Overlay, Container } from './styles';

export default function Modal({ children }) {
  return (
    <Overlay>
      <Container>
        {children}
      </Container>
    </Overlay>
  );
}

Modal.propTypes = {
  children: PropTypes.node.isRequired,
};
