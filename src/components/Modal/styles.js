import styled from 'styled-components';

export const Overlay = styled.div`
  background: rgba(500, 500, 500, 0.6) ;
  backdrop-filter: blur(1px);
  position: fixed;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Container = styled.div`
  background: none;
  position: fixed;
  left: 100px;
  top: 120px;
  width: auto;
  min-width: 150px;
  height: auto;
  min-height: 50px;
  background: #FFFFFF;
  border: 2px solid ${({ theme }) => theme.colors.black.light};;
  border-radius: 4px;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.04);
`;
