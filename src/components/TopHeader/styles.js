import styled from 'styled-components';

export const Container = styled.div`
  background: ${({ theme }) => theme.colors.black.light};
  color: #FFFFFF;
  display: flex;
  justify-content: center;
  padding: 0 20px;

  div {
    max-width: ${({ theme }) => theme.responsive[1440]};
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: end;
    font-size: 14px;
    padding: 7px 0;
  }

  a {
    color: #FFFFFF;
    font-weight: bold;
  }

  span {
    margin: 0 3px;
  }
`;
