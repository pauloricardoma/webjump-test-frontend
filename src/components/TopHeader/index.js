import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { Container } from './styles';

export default function TopHeader({ title1, title2 }) {
  return (
    <Container>
      <div>
        <Link to="/">{title1}</Link>
        <span>ou</span>
        <Link to="/">{title2}</Link>
      </div>
    </Container>
  );
}

TopHeader.propTypes = {
  title1: PropTypes.string.isRequired,
  title2: PropTypes.string.isRequired,
};
