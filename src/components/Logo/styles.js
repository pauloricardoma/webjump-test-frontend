import styled from 'styled-components';

export const LogoContainer = styled.div`
  font-size: 24px;
  font-weight: 900;
  font-family: 'Courier New', Courier, monospace;
  letter-spacing: -4px;
  display: flex;
  align-items: center;
  justify-content: center;

  @media (max-width: 564px) {
    margin: 0 20px;
  }

  .web {
    color: ${({ theme }) => theme.colors.red.mid};
  }

  .jump {
    color: ${({ theme }) => theme.colors.black.mid};
  }

  .exclam {
    color: ${({ theme }) => theme.colors.red.mid};
    font-size: 80px;
    margin-left: -10px;
    transform: rotate(-160deg);
    margin-bottom: 12px;
  }
`;
