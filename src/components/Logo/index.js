import React from 'react';

import { LogoContainer } from './styles';

export default function Logo() {
  return (
    <LogoContainer>
      <h1 className="web">WEB</h1>
      <h1 className="jump">JUMP</h1>
      <h1 className="exclam">!</h1>
    </LogoContainer>
  );
}
