import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  body {
    background: ${({ theme }) => theme.colors.background};
    color: ${({ theme }) => theme.colors.black.dark};
    font-family: 'Open Sans';
    font-size: 16px;
  }

  button {
    cursor: pointer;
  }
`;
