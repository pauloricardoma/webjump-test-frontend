export default {
  colors: {
    background: '#FFFFFF',
    black: {
      light: '#231F20',
      mid: '#1A1818',
      dark: '#000000',
    },
    grey: {
      200: '#E2DEDC',
      300: '#ACACAC',
      400: '#959595',
      500: '#8E8E8E',
      600: '#808185',
      900: '#626262',
    },
    red: {
      orange: '#F26324',
      light: '#ED1A39',
      mid: '#E50019',
      dark: '#CB0D1F',
    },
    blue: {
      button: '#00A8A9',
      filter: '#27A3A9',
      dark: '#1E2B50',
    },
  },

  responsive: {
    1440: '1260px',
    1024: '1004px',
    768: '749px',
    320: '310px',
  },
};
