/* eslint-disable max-len */
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { FaQuestion } from 'react-icons/fa';

import {
  BigContainer, ModalContainer, Header,
  SmallContainer, DivMain, ContainerMain,
  Aside, Main, Section,
} from './styles';

import CategoriesService from '../../services/CategoriesService';
import Loader from '../../components/Loader';
import Nav from '../../components/Nav';
import Logo from '../../components/Logo';
import Button from '../../components/Button';
import Input from '../../components/Input';
import Modal from '../../components/Modal';
// import delay from '../../utils/delay';

export default function Home() {
  const [categories, setCategories] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [modalLinkON, setModalLinkON] = useState(false);

  function handleModalLinkON() {
    setModalLinkON(
      (prevState) => (prevState === false),
    );
  }

  useEffect(() => {
    async function loadCategories() {
      try {
        setIsLoading(true);

        const response = await CategoriesService.listCategories();

        setCategories(response.items);
      } catch (err) {
        console.log('Caiu no catch', err);
      } finally {
        setIsLoading(false);
      }
    }
    loadCategories();
  }, []);

  return (
    <BigContainer
      onClickCapture={() => (setModalLinkON(false))}
    >
      <Loader isLoading={isLoading} />
      {modalLinkON && (
        <Modal>
          <ModalContainer>
            <ul>
              <li>
                <Link to="/">
                  Página Inicial
                </Link>
              </li>
              {categories.map((category) => (
                <li key={category.id}>
                  <Link to={`/${category.id}`}>
                    {category.name}
                  </Link>
                </li>
              ))}
              <li>
                <Link to="/">
                  Contato
                </Link>
              </li>
            </ul>
          </ModalContainer>
        </Modal>
      )}

      <Header>
        <SmallContainer>
          <button
            type="button"
            className="button-fa-question-black"
            onClick={handleModalLinkON}
          >
            <FaQuestion className="fa-question-black" />
          </button>

          <Logo />

          <div className="search">
            <Input className="input-none" type="text" />
            <Button
              className="button-none"
              type="button"
              category={false}
            >
              BUSCAR
            </Button>
          </div>

          <button
            type="button"
            className="button-fa-question-red"
          >
            <FaQuestion className="fa-question-red" />
          </button>
        </SmallContainer>
      </Header>

      <Nav />

      <DivMain>
        <Aside>
          <ul>
            <li>Página Inicial</li>
            {categories.map((category) => (
              <li key={category.id}>
                <Link to={`/${category.id}`}>
                  {category.name}
                </Link>
              </li>
            ))}
            <li>Contato</li>
          </ul>
        </Aside>

        <ContainerMain>
          <Section />
          <Main>
            <h2>Seja bem-vindo!</h2>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. At molestias placeat esse aperiam rerum, totam cum laudantium quia illo animi cupiditate earum, veniam reprehenderit ipsum perspiciatis voluptates! Adipisci, distinctio tenetur? Lorem ipsum dolor sit amet consectetur adipisicing elit. At molestias placeat esse aperiam rerum, totam cum laudantium quia illo animi cupiditate earum, veniam reprehenderit ipsum perspiciatis voluptates! Adipisci, distinctio tenetur? Lorem ipsum dolor sit amet consectetur adipisicing elit. At molestias placeat esse aperiam rerum, totam cum laudantium quia illo animi cupiditate earum, veniam reprehenderit ipsum perspiciatis voluptates! Adipisci, distinctio tenetur? Lorem ipsum dolor sit amet consectetur adipisicing elit. At molestias placeat esse aperiam rerum, totam cum laudantium quia illo animi cupiditate earum, veniam reprehenderit ipsum perspiciatis voluptates! Adipisci, distinctio tenetur? Lorem ipsum dolor sit amet consectetur adipisicing elit. At molestias placeat esse aperiam rerum, totam cum laudantium quia illo animi cupiditate earum, veniam reprehenderit ipsum perspiciatis voluptates! Adipisci, distinctio tenetur? Lorem ipsum dolor sit amet consectetur adipisicing elit. At molestias placeat esse aperiam rerum.</p>
          </Main>
        </ContainerMain>
      </DivMain>

    </BigContainer>
  );
}
