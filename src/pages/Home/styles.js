import styled from 'styled-components';

export const ModalContainer = styled.div`
  background: ${({ theme }) => theme.colors.grey[200]};
  padding: 20px;
  padding-left: 30px;

  ul {

    & li {
      margin: 4px 0;
    }
  }
  a {
    text-decoration: none;
    color: ${({ theme }) => theme.colors.black.dark};
  }
`;

export const BigContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Header = styled.header`
  width: 100%;
  height: 114px;
  display: flex;
  justify-content: center;
  padding: 0 20px;
`;

export const SmallContainer = styled.div`
  width: 100%;
  height: 114px;
  max-width: ${({ theme }) => theme.responsive[1440]};
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media (max-width: 768px) {
    justify-content: space-around;
    flex-direction: column;
  }

  @media (max-width: 564px) {
    justify-content: center;
    flex-direction: row;
  }

  .search {
    display: flex;
    flex-direction: row;

    & input, button {
      @media (max-width: 564px) {
        display: none;
      }
    }
  }

  .button-fa-question-black {
    display: none;
    background: transparent;
    padding: 2px;
    border: 4px solid ${({ theme }) => theme.colors.black.light};
    border-radius: 4px;

    @media (max-width: 564px) {
      display: flex;
    }

    .fa-question-black {
      color: ${({ theme }) => theme.colors.black.light};
      width: 25px;
      height: 23px;
    }

    &:active {
      background: ${({ theme }) => theme.colors.black.light};

      .fa-question-black {
        color: #FFFFFF;
      }
    }
  }

  .button-fa-question-red {
    display: none;
    background: transparent;
    padding: 2px;
    border: 4px solid ${({ theme }) => theme.colors.red.dark};
    border-radius: 4px;

    @media (max-width: 564px) {
      display: flex;
    }

    .fa-question-red {
      color: ${({ theme }) => theme.colors.red.dark};
      width: 25px;
      height: 23px;
    }

    &:active {
      background: ${({ theme }) => theme.colors.red.dark};

      .fa-question-red {
        color: #FFFFFF;
      }
    }
  }
`;

export const DivMain = styled.main`
  width: 100%;
  height: 100%;
  max-width: ${({ theme }) => theme.responsive[1440]};
  display: flex;
  flex-direction: row;
  margin: 24px 20px;
  padding: 0 20px;

  @media (max-width: 768px) {
    display: flex;
    flex-direction: column;
  }

  @media (max-width: 564px) {
    margin-top: 0;
  }
`;

export const ContainerMain = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

export const Aside = styled.aside`
  background: ${({ theme }) => theme.colors.grey[200]};
  width: 100%;
  max-width: 261px;
  max-height: 474px;
  margin-right: 20px;

  @media (max-width: 768px) {
    display: none;
  }

  ul {
    font-size: 18px;
    letter-spacing: 1px;
    padding-top: 22px;
    padding-left: 27px;

    li {

      + li {
        margin-top: 3px
      }

      a {
        text-decoration: none;
        color: ${({ theme }) => theme.colors.black.dark};
      }
    }
  }
`;

export const Main = styled.div`
  width: 100%;
  max-width: 953px;

  h2 {
    font-size: 26px;
    margin-top: 16px;
    margin-bottom: 7px;
    font-weight: 300;
    color: ${({ theme }) => theme.colors.black.dark};
  }
`;

export const Section = styled.section`
  width: 100%;
  background: ${({ theme }) => theme.colors.grey[300]};
  min-height: 281px;
  max-width: 953px;

  @media (max-width: 1024px) {
    min-height: 281px;
  }

  @media (max-width: 768px) {
    min-height: 86px;
  }
`;
