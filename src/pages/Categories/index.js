import React, { useEffect, useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import { Link } from 'react-router-dom';

import {
  FaTh, FaList, FaBars, FaShoppingCart,
} from 'react-icons/fa';
import {
  BigContainer, Header, NavContainer, SmallContainer,
  DivMain, ContainerMain, Aside, Main,
  Section, Card,
} from './styles';

import Nav from '../../components/Nav';
import Modal from '../../components/Modal';
import CategoriesService from '../../services/CategoriesService';
import ProductsService from '../../services/ProductsService';
import Button from '../../components/Button';
import Input from '../../components/Input';
import Logo from '../../components/Logo';
import Loader from '../../components/Loader';

import ordenar from '../../utils/ordenar';

export default function Categories({ match }) {
  const id = get(match, 'params.id', '');
  const [isLoading, setIsLoading] = useState(true);
  const [categories, setCategories] = useState([]);
  const [orderBy, setOrderBy] = useState('Preço (asc)');
  const [products, setProducts] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [modalMenuON, setModalMenuON] = useState(false);
  // const [listarProducts, setListProducts] = useState(products);
  // const [categoryName, setCategoryName] = useState('');

  const categoryFilterById = useMemo(() => categories.filter((category) => (
    category.id === Number(id)
  )), [categories, id]);

  const filteredProducts = useMemo(() => products.filter((product) => (
    product.name.toLowerCase().includes(searchTerm.toLowerCase())
  )).sort((a, b) => {
    switch (orderBy.toString()) {
      case 'Preço (asc)':
        return ordenar(a.price, b.price);
      case 'Preço (desc)':
        return -ordenar(a.price, b.price);
      default:
        return ordenar(a.price, b.price);
    }
  }), [products, searchTerm, orderBy]);

  function handleChangeSearchTerm(event) {
    setSearchTerm(event.target.value);
  }

  function handleToggleOrderPrice() {
    setOrderBy(
      (prevState) => (prevState === 'Preço (desc)' ? 'Preço (asc)' : 'Preço (desc)'),
    );
  }

  function handleAsideFilterTipo(event) {
    setSearchTerm(event.currentTarget.value);
  }

  function handleModalMenuON() {
    setModalMenuON(
      (prevState) => (prevState === false),
    );
  }

  useEffect(() => {
    async function loadList() {
      try {
        setIsLoading(true);

        const dataProducts = await ProductsService.listProducts(id);
        const dataCategory = await CategoriesService.listCategories();

        setCategories(dataCategory.items);

        setProducts(dataProducts.items, dataProducts.filters);
      } catch (err) {
        console.log('Caiu no catch', err);
      } finally {
        setIsLoading(false);
      }
    }
    loadList();
  }, [id]);

  return (
    <BigContainer
      onClickCapture={() => (setModalMenuON(false))}
    >
      <Loader isLoading={isLoading} />
      {modalMenuON && (
        <Modal>
          <NavContainer>
            <ul>
              <li><Link to="/">PÁGINA INICIAL</Link></li>
              {categories.map((category) => (
                <li>
                  <Link
                    name={category.name}
                    key={category.id}
                    to={`/${category.id}`}
                  >
                    {category.name.toUpperCase()}
                  </Link>
                </li>
              ))}
              <li><Link to="/">CONTATO</Link></li>
            </ul>
          </NavContainer>
        </Modal>
      )}

      <Header>
        <SmallContainer>
          <button
            type="button"
            className="button-fa-bars"
            onClick={handleModalMenuON}
          >
            <FaBars className="fa-bars" />
          </button>
          <Logo />

          <div className="search">
            <Input
              value={searchTerm}
              type="text"
              placeholder="Pesquisar produto da seção..."
              onChange={handleChangeSearchTerm}
            />
            <Button
              type="button"
              category={categories.id === id}
              className="blue"
            >
              BUSCAR
            </Button>
          </div>
          <button type="button" className="button-fa-shoppingcart">
            <FaShoppingCart className="fa-shoppingcart" />
          </button>
        </SmallContainer>
      </Header>

      <Nav />

      <DivMain>
        <div className="apresentacao">
          <span>
            <Link to="/">Página inicial</Link>
          </span>
          <span>{' > '}</span>
          {categoryFilterById.map((category) => (
            <span key={category.id}>{category.name}</span>
          ))}
        </div>
      </DivMain>

      <DivMain>
        <Aside>
          <div className="aside-container">
            <div className="filter">FILTRE POR</div>
            <h4>CATEGORIAS</h4>
            <ul>
              {categories.map((category) => (
                <li key={category.id}>
                  <Link
                    className="categories-link"
                    to={`/${category.id}`}
                  >
                    {category.name}
                  </Link>
                </li>
              ))}
            </ul>
            <h4>CORES</h4>
            <div className="filter-buttons">
              <button className="one-filter-button" type="button">red</button>
              <button className="two-filter-button" type="button">orange</button>
              <button className="three-filter-button" type="button">azul</button>
            </div>
            <h4>TIPO</h4>
            <ul>
              <li>
                <button
                  className="filter-tipo-button"
                  type="button"
                  value="Corrida"
                  onClick={handleAsideFilterTipo}
                >
                  Corrida
                </button>
              </li>
              <li>
                <button
                  className="filter-tipo-button"
                  type="button"
                  value="Caminhada"
                  onClick={handleAsideFilterTipo}
                >
                  Caminhada
                </button>
              </li>
              <li>
                <button
                  className="filter-tipo-button"
                  type="button"
                  value="Casual"
                  onClick={handleAsideFilterTipo}
                >
                  Casual
                </button>
              </li>
              <li>
                <button
                  className="filter-tipo-button"
                  type="button"
                  value="Social"
                  onClick={handleAsideFilterTipo}
                >
                  Social
                </button>
              </li>
            </ul>
          </div>
        </Aside>

        <ContainerMain>
          <Section>
            <h3>{categoryFilterById.map((category) => category.name)}</h3>
          </Section>

          <Main>
            <div className="listagem-header">
              <div className="buttons-grid">
                <button type="button">
                  <FaTh className="fath" />
                </button>
                <button type="button">
                  <FaList className="falist" />
                </button>
              </div>
              <div className="ordenar-select">
                <span className="ordenar-titulo">ORDENAR POR</span>
                <span>
                  <select onChange={handleToggleOrderPrice}>
                    <option value="Preço (asc)">
                      Preço (asc)
                    </option>
                    <option value="Preço (desc)">
                      Preço (desc)
                    </option>
                  </select>
                </span>
              </div>
            </div>
            <div className="listagem-mid">
              {filteredProducts.map((product) => (
                <Card key={product.id}>
                  <div className="imagem-produtos">
                    <img src={product.image} alt={product.path} />
                  </div>
                  <div className="descricao-produtos">
                    <h4>{product.name}</h4>
                    <h5>{`R$${product.price}`}</h5>
                  </div>
                  <div className="button-size-lista-commerce">
                    <button className="button-lista-commerce" type="button">Comprar</button>
                  </div>
                </Card>
              ))}
            </div>
            <div className="listagem-footer" />
          </Main>
        </ContainerMain>
      </DivMain>

    </BigContainer>
  );
}

Categories.propTypes = {
  match: PropTypes.shape({}).isRequired,
  // history: PropTypes.shape([]).isRequired,
};
