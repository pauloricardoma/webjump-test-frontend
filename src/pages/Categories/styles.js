import styled from 'styled-components';

export const NavContainer = styled.nav`
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${({ theme }) => theme.colors.red.dark};
  width: 200px;
  height: 200px;

  & ul {
    list-style: none;

    & li {
      margin: 12px 0;

      & a {
        font-size: 18px;
        color: #FFFFFF;
        font-weight: 800;
        text-decoration: none;
      }
    }
  }

`;

export const BigContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Header = styled.header`
  width: 100%;
  height: 114px;
  display: flex;
  justify-content: center;
  padding: 0 20px;
`;

export const SmallContainer = styled.div`
  width: 100%;
  height: 114px;
  max-width: ${({ theme }) => theme.responsive[1440]};
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media (max-width: 768px) {
    justify-content: space-around;
    flex-direction: column;
  }

  @media (max-width: 564px) {
    justify-content: center;
    flex-direction: row;
  }

  .search {
    display: flex;
    flex-direction: row;

    & input, button {
      @media (max-width: 564px) {
        display: none;
      }
    }
  }

  .button-fa-bars {
    display: none;
    background: transparent;
    border: none;

    @media (max-width: 564px) {
      display: flex;
    }

    .fa-bars {
      color: ${({ theme }) => theme.colors.black.dark};
      width: 30px;
      height: 30px;
    }
  }

  .button-fa-shoppingcart {
    display: none;
    background: transparent;
    border: none;

    @media (max-width: 564px) {
      display: flex;
    }

    .fa-shoppingcart {
      color: ${({ theme }) => theme.colors.black.dark};
      width: 30px;
      height: 30px;
    }
  }
`;

export const DivMain = styled.main`
  width: 100%;
  height: 100%;
  max-width: ${({ theme }) => theme.responsive[1440]};
  display: flex;
  flex-direction: row;
  margin: 24px;
  padding: 0 20px;

  @media (max-width: 564px) {
    justify-content: center;
    flex-direction: column;
    margin: 8px 0;
  }

  .apresentacao {
    font-size: 14px;

    @media (max-width: 768px) {
      margin-top: 20px;
    }

    @media (max-width: 564px) {
      margin: 0;
    }

    a {
      color: ${({ theme }) => theme.colors.blue.button};
      text-decoration: none;
    }

    & span {
      color: ${({ theme }) => theme.colors.blue.button};

      &:last-child {
        color: ${({ theme }) => theme.colors.red.dark};
      }
    }
  }
`;

export const ContainerMain = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

export const Aside = styled.aside`
  background: #FFFFFF;
  width: 100%;
  max-width: 261px;
  max-height: 474px;
  border: 1px solid ${({ theme }) => theme.colors.grey[200]};
  margin-right: 20px;

  @media (max-width: 1024px) {
    max-width: 226px;
  }

  @media (max-width: 768px) {
    max-width: 207px;
  }

  @media (max-width: 564px) {
    margin-bottom: 20px;
  }

  @media (max-width: 320px) {
    max-width: 100%;
  }

  .aside-container {
    padding-top: 14px;
    padding-left: 17px;
  }

  .filter {
    color: ${({ theme }) => theme.colors.red.light};
    font-size: 24px;
    font-weight: bold;
    margin-bottom: 14px;
  }

  h4 {
    color: ${({ theme }) => theme.colors.blue.dark};
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 12px;
  }

  ul {
    margin-left: 17px;
    margin-bottom: 40px;
    color: ${({ theme }) => theme.colors.grey[900]};

    li {
      a {
        text-decoration: none;
        color: ${({ theme }) => theme.colors.grey[900]};
      }

      + li {
        margin-top: 4px;
      }
    }
  }

  .filter-buttons {
    margin-bottom: 32px;

    button {
      width: 48px;
      height: 24px;
      border: none;

      + button {
        margin-left: 3px;
      }
    }

    .one-filter-button {
      color: ${({ theme }) => theme.colors.red.dark};
      background: ${({ theme }) => theme.colors.red.dark};
    }

    .two-filter-button {
      color: ${({ theme }) => theme.colors.red.orange};
      background: ${({ theme }) => theme.colors.red.orange};
    }

    .three-filter-button {
      color: ${({ theme }) => theme.colors.blue.filter};
      background: ${({ theme }) => theme.colors.blue.filter};
    }
  }

  .filter-tipo-button {
    background: transparent;
    border: none;
    color: ${({ theme }) => theme.colors.grey[900]};
    font-size: 16px;
  }
`;

export const Section = styled.section`
  width: 100%;
  background: #FFFFFF;
  max-width: 953px;
  display: flex;
  flex-direction: column;

  h3 {
    font-size: 32px;
    color: ${({ theme }) => theme.colors.red.light};
    font-weight: 400;

  }

  &::after {
    content: '';
    height: 1px;
    width: 100%;
    background: ${({ theme }) => theme.colors.grey[200]};
    margin-top: 7px;
    margin-bottom: 12px;
  }
`;

export const Main = styled.div`
  width: 100%;
  max-width: 953px;

  .listagem-header {
    width: 100%;
    height: 27px;
    display: flex;
    align-items: center;
    justify-content: space-between;

    .buttons-grid {
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin-right: 8px;
    }

    button {
      border: none;
      background: transparent;
      height: 20px;
      width: 20px;

      + button {
        margin-left: 12px;
      }
    }

    .fath {
      color: ${({ theme }) => theme.colors.red.light};
      height: 20px;
      width: 20px;
    }

    .falist {
      color: ${({ theme }) => theme.colors.blue.button};
      height: 20px;
      width: 20px;
    }

    .ordenar-select {
      display: flex;
      flex-direction: row;
      justify-content: end;
      align-items: center;

      .ordenar-titulo {
        color: ${({ theme }) => theme.colors.grey[600]};
        font-size: 11px;
        margin-right: 14px;

        @media (max-width: 564px) {
          display: none;
        }
      }

      select {
        width: 216px;
        outline: none;

        option {
          font-size: 14px;
          color: ${({ theme }) => theme.colors.black.dark};
        }
      }
    }
  }

  .listagem-mid {
    margin-top: 45px;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
  }
`;

export const Card = styled.div`
  max-height: 365px;
  max-width: 212px;
  min-height: 254px;
  min-width: 132px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  margin: 5px 0;

  @media (max-width: 1024px) {
    max-width: 164px;
    max-height: 314px;
  }

  @media (max-width: 320px) {
    max-width: 134px;
    max-height: 256px;
  }

  .imagem-produtos {
    width: 100%;
    height: 100%;
    max-height: 233px;
    max-width: 212px;
    border: 1px solid ${({ theme }) => theme.colors.grey[200]};
    display: flex;
    justify-content: center;
    align-items: center;
    overflow: hidden;
    padding: 9px 8px;

    & img {
      height: 100%;
      width: 100%;
    }
  }

  .descricao-produtos {
    width: 100%;
    height: 100%;
    max-height: 100px;
    min-height: 90px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    text-align: center;

    h4 {
      text-transform: uppercase;
      color: ${({ theme }) => theme.colors.grey[900]};
      font-weight: 400;
      margin-top: 8px;
    }

    h5 {
      letter-spacing: 2px;
      font-size: 21px;
      font-weight: 900;
      color: ${({ theme }) => theme.colors.blue.dark};
      margin-top: 24px;
    }
  }

  .button-size-lista-commerce {
    width: 100%;

    .button-lista-commerce {
      width: 100%;
      height: 40px;
      min-height: 40px;
      border: none;
      border-radius: 5px;
      text-transform: uppercase;
      font-size: 18px;
      color: #FFFFFF;
      background: ${({ theme }) => theme.colors.blue.button};
      transition: background 0.2s ease-in;

      &:hover {
        background: ${({ theme }) => theme.colors.blue.filter};
      }

      &:active {
        background: ${({ theme }) => theme.colors.blue.dark};
      }
    }
  }
`;
