export default function ordenar(ord1, ord2) {
  if (ord1 < ord2) {
    return -1;
  } if (ord1 > ord2) {
    return 1;
  }
  return 0;
}
