import HttpClient from '../utils/HttpClient';

class ProductsService {
  constructor() {
    this.httpClient = new HttpClient('http://localhost:8888/api/V1/categories');
  }

  async listProducts(id) {
    return this.httpClient.get(`/${id}`);
  }
}

export default new ProductsService();
