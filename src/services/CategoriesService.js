import HttpClient from '../utils/HttpClient';

class CategoriesService {
  constructor() {
    this.httpClient = new HttpClient('http://localhost:8888/api/V1/categories');
  }

  async listCategories() {
    return this.httpClient.get('/list');
  }
}

export default new CategoriesService();
