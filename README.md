# WEBJUMP | Fase de Testes Desenvolvedor(a) Front-end

Projeto foi criado com o uso da tecnologia ReactJS, e para o CSS foi usado Styled Components.

## Scripts para rodar projeto

Necessário fazer um clone do repositório e instalar todas as depências do node_modules, no diretório do projeto

### `yarn add`

ou

### `npm install`

Para inicializar o mock e usar a API do backend abrir terminal no diretório do repositório e iniciar o script

### `yarn dev`

O backend será iniciado em
Abrir para categorias: [http://localhost:8888/api/V1/categories/list]
Abrir para produtos, id de categorias: [http://localhost:8888/api/V1/categories/{id}]

-----------------------------------------------------------------------------------------------------------------

Enquanto roda o backend em paralelo

Para inicializar o frontend para vizualização do projeto e já fazer as chamadas na API abrir novo terminal no diretório do repositório e iniciar o script

### `yarn start`

O frontend será iniciado automaticamente em
Abrir: [http://localhost:3000]

-----------------------------------------------------------------------------------------------------------------

<h4>🛠 Tecnologias</h4>

As seguintes ferramentas foram usadas na construção do projeto:

<img src='https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E' />
<img src='https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB' />


